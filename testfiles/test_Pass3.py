__author__ = 'brinschw'

"""
Created on Sun Jun 11 17:28:35 2017
@author: kurtb_000
"""

import unittest
from checkfiles import *

if __name__ == '__main__':
    #unittest.main()
    PATH = r"H:\Work\Battleships\testfiles\Pass3\\"
    fname_fleet = PATH + 'Fleet file.txt'
    fname_moves = PATH + 'Move file.txt'
    fname_place = PATH + 'Placement file.txt'


    fleet = read_fleet(fname_fleet)
    (fleet_ok, fleet_errs) = is_fleet_feasible(fleet)
    if not fleet_ok:
        for e in fleet_errs:
            print(e)

    moves = read_moves(fname_moves)
    (moves_ok, moves_errs) = is_moves_feasible(moves)
    if not moves_ok:
        for e in moves_errs:
            print(e)

    place = read_place(fname_place)
    (place_ok, place_errs) = is_place_feasible(place, fleet)
    if not place_ok:
        for e in place_errs:
            print(e)
